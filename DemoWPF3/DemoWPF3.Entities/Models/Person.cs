﻿using DemoWPF3.Entities.Interfaces;
using DemoWPF3.Tools.Misc;
using DemoWPF3.Tools.Serialization;
using System;

namespace DemoWPF3.Entities.Models
{
    [Serializable]
    public class Person:IToFile, IFromFile<Person>
    {
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public DateTime Publicacion { get; set; }
        public string Categoria { get; set; }
        public int Existencias { get { return Util.GetAge(Publicacion); } }

        public Person FromBinary(string filepath)
        {
           return BinarySerialization.ReadFromBinaryFile<Person>(filepath);
        }

        public Person FromJson(string filepath)
        {
            return JsonSerialization.ReadFromJsonFile<Person>(filepath);
        }

        public Person FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Person>(filepath);
        }

        public void ToBinary(string filepath)
        {
            BinarySerialization.WriteToBinaryFile(filepath, this);
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
