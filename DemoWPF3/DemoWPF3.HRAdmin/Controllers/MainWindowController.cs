﻿using DemoWPF3.Entities.Models;
using DemoWPF3.HRAdmin.ViewModels;
using DemoWPF3.HRAdmin.Views.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DemoWPF3.HRAdmin.Controllers
{
    public class MainWindowController
    {
        private MainWindow mainWindow;
        public MainWindowController(MainWindow window)
        {
            mainWindow = window;
        }
        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;
                case "PersonMenuItem":
                    mainWindow.DataContext = new PersonViewModel();
                    break;
                case "PersonListMenuItem":
                    mainWindow.DataContext = new GroupViewModel() { Title = "Título de la ventana", Group = GetGroup() };
                    break;


            }
        }
        private Group GetGroup()
        {
            Group g = new Group
            {
                Name = "Los 4 Fantásticos",
                Members = new List<Person>
                    {
                        new Person() { Titulo = "Calculo Diferencial y Experimental", Autor = "Erick Nasledov", Publicacion = new DateTime(2000, 1, 1), Categoria = "Matematicas" },
                        new Person() { Titulo = "Programacion POO", Autor = "Luwing Stward", Publicacion = new DateTime(1985, 1, 1), Categoria = "Informatica" },
                        new Person() { Titulo = "Conquista de America", Autor = "Fernando Salazar", Publicacion = new DateTime(1985, 1, 1),Categoria="Historia" },
                        new Person() { Titulo = "Edad Media", Autor = "Richard Hathaway", Publicacion = new DateTime(1980, 1, 1), Categoria="Historia" }
                    }
            };
            return g;
        }
    }
}
