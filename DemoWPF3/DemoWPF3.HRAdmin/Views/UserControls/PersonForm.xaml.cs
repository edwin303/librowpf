﻿using DemoWPF3.Entities.Interfaces;
using DemoWPF3.Entities.Models;
using DemoWPF3.HRAdmin.Controllers;
using System;
using System.Windows;
using System.Windows.Controls;

namespace DemoWPF3.HRAdmin.Views.UserControls
{
    /// <summary>
    /// Interaction logic for PersonForm.xaml
    /// </summary>
    public partial class PersonForm : UserControl, IForm<Person>
    {
        PersonController pc;
        public PersonForm()
        {
            InitializeComponent();
            SetupController();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Person GetData()
        {
            Person person = new Person
            {
                Titulo = TituloTextBox.Text,
                Autor = AutorTextBox.Text,
                Publicacion = (DateTime)PublicacionPicker.SelectedDate,
                //person.State = ((ComboBoxItem)StateCombobox.SelectedValue).Content.ToString();
                Categoria = CategoriaCombobox.SelectedValue.ToString()
            };
            return person;
        }

        public void SetData(Person data)
        {
            TituloTextBox.DataContext = data;
            AutorTextBox.DataContext = data;
            PublicacionPicker.DataContext = data;
            CategoriaCombobox.DataContext = data;
        }

        protected void SetupController()
        {
            pc = new PersonController(this);
            this.SaveButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }
        public void Hide()
        {
            this.Visibility = Visibility.Collapsed;
        }
    }
}
