﻿using DemoWPF3.Entities.Models;
using DemoWPF3.HRAdmin.Controllers;
using System;
using System.Windows;
using DemoWPF3.Entities.Interfaces;

namespace DemoWPF3.HRAdmin.Views.Windows
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class PersonWindow : Window, IForm<Person>
    {
        PersonController pc;
        public PersonWindow()
        {
            InitializeComponent();
            //centrar la ventana
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //Inicializar controlador
            SetupController();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Person GetData()
        {
            Person person = new Person();
            person.Titulo = TituloTextBox.Text;
            person.Autor = AutorTextBox.Text;
            person.Publicacion = (DateTime)PublicacionPicker.SelectedDate;
            //person.State = ((ComboBoxItem)StateCombobox.SelectedValue).Content.ToString();
            person.Categoria = CategoriaCombobox.SelectedValue.ToString();
            return person;
        }

        public void SetData(Person person)
        {
            TituloTextBox.Text = person.Titulo;
            AutorTextBox.Text = person.Autor;
            PublicacionPicker.SelectedDate = person.Publicacion;
            CategoriaCombobox.SelectedValue = person.Categoria;
        }

        protected void SetupController()
        {
            pc = new PersonController(this);
            this.SaveButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }
    }
}
