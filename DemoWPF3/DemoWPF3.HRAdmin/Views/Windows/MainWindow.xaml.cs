﻿using DemoWPF3.HRAdmin.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DemoWPF3.HRAdmin.Views.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowController mc;
        public MainWindow()
        {
            InitializeComponent();
            SetupController();
        }
        public void SetupController()
        {
            mc = new MainWindowController(this);
            this.Loaded += new RoutedEventHandler(mc.MainWindowEventHandler);
            RoutedEventHandler routed = new RoutedEventHandler(mc.MainMenuEventHandler);
            this.exitAplicationMenutItem.Click += routed;
            this.PersonMenuItem.Click += routed;
            this.PersonListMenuItem.Click += routed;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
